package exercise;

class App {
    public static void numbers() {
        // BEGIN
        Number x = 3;
        System.out.println(System.out.println(x / 0));
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        System.out.println(language + " " + "works" + " " + "on" + " " + "JVM");
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }
}
